import './App.css';
import Nav from './components/Nav';
import Home from './components/Home';
import ReleaseAdd from './components/ReleaseAdd';
import ReleaseList from './components/ReleaseList';
import ReleaseUpdate from './components/ReleaseUpdate';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  
} from "react-router-dom";

function App() {
  return (
    <Router>
      <div>
        <Nav />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/releaseadd" component={ReleaseAdd} />
          <Route path="/releaselist" exact component={ReleaseList} />
          <Route path="/releaselist/:id" component={ReleaseUpdate} />
        </Switch>

      </div>
    </Router>
  );
}

export default App;
