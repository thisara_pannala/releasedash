import { Avatar, Grid, Paper } from '@material-ui/core';
import React from 'react';
//import AddBoxIcon from '@material-ui/icons/AddBox';
import axios from 'axios';
import EditIcon from '@material-ui/icons/Edit';
import SuccessAlert from './SuccessAlert';
import ErrorAlert from './ErrorAlert';

class ReleaseUpdate extends React.Component {
    constructor(){
        super();
        this.state={
            release_name:'',
            release_status:'',
            release_version:'',
            release_client:'',
            alert_message: ''
        }
        this.changeHandler=this.changeHandler.bind(this);
        this.submitHandler=this.submitHandler.bind(this);
    }
   
    changeHandler=(event)  =>{
       this.setState({
            [event.target.name]:event.target.value })
        }
        
    
    submitHandler = (event) => {
        event.preventDefault()
        axios.put(`http://localhost:4000/api/release/${this.props.location.state.data}`, this.state)
        .then(response => {
            this.setState({alert_message: "success"})
        }).catch(error=>{
            this.setState({alert_message: "error"})
        })    
            //console.log("data updaing", response)
       
    }

    componentDidMount() {
        axios.get(`http://localhost:4000/api/release/${this.props.location.state.data}`)
            .then(response => {
                const release = response.data;
                this.setState({ 
                    release_client: release.release_client,
                    release_name: release.release_name,
                    release_version: release.release_version,
                    release_status: release.release_status });
                console.log("data get my Id",release)

            })
    };

    render() {
        
        return (
            <Grid>
                <hr/>
                {this.state.alert_message==="success"?<SuccessAlert/>:null}
                {this.state.alert_message==="error"?<ErrorAlert/>:null}
                <Paper elevation={20} style={{padding: '20px', height: '70vh', width:  '400px', margin: '20px auto'}}>
                <Grid align="center">
                    <Avatar ><EditIcon /></Avatar>
                    <h2>Update Release</h2>
                </Grid>
                <form onSubmit={this.submitHandler}>
                <div>
                    <label>Release Name</label>
                    <div>
                    <input 
                    type='text' 
                    size="50"
                    name ='release_name' 
                    onChange={this.changeHandler}
                    value={this.state.release_name}></input>
                    </div>
                </div>
                <br/>
                <div>
                    <label>Release Version</label>
                    <div>
                    <input 
                    type='text' size="50"
                    name ='release_version' 
                        onChange={this.changeHandler}
                    value={this.state.release_version}></input>
                    </div>
                </div>
                <br/>
                <div>
                    <label>Release Status</label>
                    <div>
                    <input 
                    type='text' size="50"
                    name ='release_status' 
                    onChange={this.changeHandler}
                    value={this.state.release_status}></input>
                    </div>
                </div>
                <br/>
                <div>
                    <label>Release Client</label>
                    <div>
                    <input 
                    type='text' size="50"
                    name ='release_client' 
                    onChange={this.changeHandler}
                    value={this.state.release_client}></input>
                    </div>
                </div>
                <br/><br/>
                <button type='submit' className='submitbutton'>Update Now</button>
                </form>
                </Paper>
            </Grid>
            
              
        );
    }
}

export default ReleaseUpdate;




