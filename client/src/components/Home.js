import { Avatar, Grid, Paper, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
//import AddBoxIcon from '@material-ui/icons/AddBox';
import DashboardIcon from '@material-ui/icons/Dashboard';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';

const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 3,
    },
    paper: {
        height: 105,
        width: 200,
    },

}));

const Home = () => {
    const classes = useStyles();
    const [all, setAll] = useState(null);
    const [completed, setCompleted] = useState(null);
    const [pending, setPending] = useState(null);
    

    useEffect(() => {
        axios.get('http://localhost:4000/api/release/count')
            .then(response => {setAll(response.data.allcount)})
            .catch(err => {console.log(err)});

        axios.get('http://localhost:4000/api/release/completed')
            .then(response => {setCompleted(response.data.completedcount)})
            .catch(err => {console.log(err)});

        axios.get('http://localhost:4000/api/release/pending')
            .then(response => {setPending(response.data.pendingcount)})
            .catch(err => {console.log(err)});        
    }, [])

    

  
   

    return (
        <Grid>
            <Paper elevation={20} style={{ padding: '20px', height: '70vh', width: '500px', margin: '20px auto' }}>
                <Grid align="center" >
                    <Avatar ><DashboardIcon /></Avatar>
                    <h2>Release Dashboard</h2>
                    <Grid container direction="column" className={classes.root} alignItems="center" justify="space-between" spacing={3} padding="100%">
                        <Grid item>
                            <Paper className={classes.paper} style={{ backgroundColor: '#fe657a' }}>
                                <Typography style={{ fontFamily: "Calibri ", fontSize: 20, fontStyle: "Bold", color: "black" }} align="center"> All Releases </Typography>
                                <Typography style={{ fontSize: 40, color: "black" }} align="center"> {all} </Typography>
                            </Paper>
                        </Grid>
                        <Grid item>
                            <Paper className={classes.paper} style={{ backgroundColor: '#06cd88' }}>
                                <Typography style={{ fontFamily: "Calibri ", fontSize: 20, fontStyle: "Bold", color: "black" }} align="center"> Completed Releases </Typography>
                                <Typography style={{ fontSize: 40, color: "black" }} align="center"> {completed} </Typography>
                            </Paper></Grid>
                        <Grid item>
                            <Paper className={classes.paper} style={{ backgroundColor: '#fd9b6d' }}>
                                <Typography style={{ fontFamily: "Calibri ", fontSize: 20, fontStyle: "Bold", color: "black" }} align="center"> Pending Releases </Typography>
                                <Typography style={{ fontSize: 40, color: "black" }} align="center"> {pending} </Typography>
                            </Paper>
                        </Grid>


                    </Grid>
                </Grid>
            </Paper>
        </Grid>
    );
}

export default Home;