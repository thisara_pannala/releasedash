//import React from 'react'
import Alert from '@material-ui/lab/Alert';
import React, { Component } from "react";

export default  class SuccessAlert extends Component {
    render(){
        return(
        <div>
            <Alert severity="success">
                Operation is successful 
                </Alert>
        </div>
        )
    };
}