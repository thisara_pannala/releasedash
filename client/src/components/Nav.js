import React from 'react';
import './Nav.css';
import { Link } from 'react-router-dom';

const Nav=()=>{

    const navStyle = {
        color: 'white'
    };


    return (
        <nav>
            <Link style={navStyle} to='/'>
                <h3 style={{color:'white' }}>Home</h3>
            </Link>

            <ul className="nav-links">
                <Link style={navStyle} to='/releaselist'>
                    <li>ReleaseLists</li>
                </Link>
                <Link style={navStyle} to='/releaseadd'>
                    <li>Create a new Release</li>
                </Link>

            </ul>
        </nav>
    );
}

export default Nav;