import { Avatar, Grid, Paper } from '@material-ui/core';
import React from 'react';
import AddBoxIcon from '@material-ui/icons/AddBox';
import axios from 'axios';
import SuccessAlert from './SuccessAlert';
import ErrorAlert from './ErrorAlert';
//import {Link} from 'react-router-dom';

class ReleaseAdd extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            release_name: '',
            release_version: '' ,
            release_status: '',
            release_client: '',
            alert_message: ''  
        }
      }
    
      changeHandler=(e)  =>{
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    
    submitHandler = (e) => {
        e.preventDefault()
        axios.post('http://localhost:4000/api/release/', this.state)
        .then(response => {
            this.setState({alert_message: "success"})
        }).catch(error=>{
            this.setState({alert_message: "error"})
        }) 
            //console.log("data insertion", response, response.data)
        
    }
    
   
    render() {
        const {release_name, release_status, release_version, release_client} = this.state
        return (
            <Grid>
                <hr/>
                {this.state.alert_message==="success"?<SuccessAlert/>:null}
                {this.state.alert_message==="error"?<ErrorAlert/>:null}
                <Paper elevation={20} style={{padding: '20px', height: '70vh', width:  '400px', margin: '20px auto'}}>
                <Grid align="center">
                    <Avatar ><AddBoxIcon /></Avatar>
                    <h2>Add New Release</h2>
                </Grid>
                <form onSubmit={this.submitHandler}>
                <div>
                    <label>Release Name</label>
                    <div>
                    <input 
                    type='text' 
                    size="50"
                    name ='release_name' 
                    onChange={this.changeHandler}
                    value={release_name}></input>
                    </div>
                </div>
                <br/>
                <div>
                    <label>Release Version</label>
                    <div>
                    <input 
                    type='text' size="50"
                    name ='release_version' 
                        onChange={this.changeHandler}
                    value={release_version}></input>
                    </div>
                </div>
                <br/>
                <div>
               
  
                    <label>Release Status</label>
                    <div>
                    <input 
                    type='text' size="50"
                    name ='release_status' 
                    onChange={this.changeHandler}
                    value={release_status}></input>
                    </div>
                </div>
                <br/>
                <div>
                    <label>Release Client</label>
                    <div>
                    <input 
                    type='text' size="50"
                    name ='release_client' 
                    onChange={this.changeHandler}
                    value={release_client}></input>
                    </div>
                </div>
                <br/><br/>
                <button type='submit' className='submitbutton'>Add Now</button>
                </form>
                </Paper>
            </Grid>
        )
    }
}



export default ReleaseAdd;