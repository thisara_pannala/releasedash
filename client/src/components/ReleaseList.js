import React from 'react';
import axios from 'axios';
//import * as ReactBootStrap from "react-bootstrap";
import { Table } from 'antd';
import Paper from '@material-ui/core/Paper';
import 'antd/dist/antd.css';
import { Button } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { green } from '@material-ui/core/colors';
import SuccessAlert from './SuccessAlert';
import ErrorAlert from './ErrorAlert';
 
class ReleaseList extends React.Component {
    state = {
        release: "",//current user entered release
        releaseList: [], //hold all the releases
        alert_message: ''  
    };

    //defining table columns
    columns = [
        { title: 'Release ID', dataIndex: 'release_id', },
        { title: 'Release Name', dataIndex: 'release_name', },
        { title: 'Release Version', dataIndex: 'release_version' },
        { title: 'Release Status', dataIndex: 'release_status', },
        { title: 'Release Clients', dataIndex: 'release_client', },
        {
            title: 'Update', dataIndex: '', key: 'x',
            render: (text, record) =>
                <Button
                    onClick={(e) => this.onUpdateClick(record.key, e)}><EditIcon style={{ color: green[500] }} />
                </Button>,
        },
        {
            title: 'Delete', dataIndex: '', key: 'x',
            render: (text, record) =>
                
                    <Button
                        onClick={(e) => { this.onDeleteClick(record.key, e);}}><DeleteIcon color="secondary" />
                    </Button>
               
        },

    ]


    componentDidMount() {
        this.getReleaseList();
    }

    //show all the tasks in the database to the user
    getReleaseList = () => {
        axios.get('http://localhost:4000/api/release/')
            .then((response) => response.data)
            .then((response) => {
                this.setState({ releaseList: response });
                console.log("data retrieval" ,response);
            });
    };

    refreshPage() {
        window.location.reload(false);
      }

    onDeleteClick = (release_id, e) => {
        e.preventDefault();
        axios.delete(`http://localhost:4000/api/release/${release_id}`)
            .then(response => {
                this.setState({alert_message: "success"})
            }).catch(error=>{
                this.setState({alert_message: "error"})
            }) 
                //console.log(response);
                //console.log("data deletion" ,response.data);
            this.refreshPage();
       this.getReleaseList();
    };

    onUpdateClick = (release_id, e) => {
        e.preventDefault();
        
        this.props.history.push({
            pathname: `/releaselist/${release_id}`,
            state:{data: release_id }});
      //  console.log(this.state.release)
    }


    render() {

        const data = [];
        this.state.releaseList.forEach(release => {
            data.push({
                key: release.release_id,
                release_id: release.release_id,
                release_name: release.release_name,
                release_version: release.release_version,
                release_status: release.release_status,
                release_client: release.release_client,


            })
        });

        return (
            <div className="tableclass">
                <h3>All Releases</h3>
                <br />
                <Paper>
                <hr/>
                {this.state.alert_message==="success"?<SuccessAlert/>:null}
                {this.state.alert_message==="error"?<ErrorAlert/>:null}
                    <Table
                        columns={this.columns}
                        dataSource={data}
                    />
                </Paper>
            </div>
        )
    }
}

export default ReleaseList;




















