# Release Dashboard

This project - Release Dashboard is a web application that provides a centralized location for all the releases and licenses.

This project is implemented using

* React.js for Frontend [https://reactjs.org/](https://reactjs.org/)	
* Express.js - Node framework for backend [https://expressjs.com/](https://expressjs.com/)	
* Ant Design UI Library [https://ant.design/](https://ant.design/)		
* MySQL for database
	
## Prerequisites

XAMPP server should be installed and Apache and MySQL should be running : [https://www.apachefriends.org/download.html](https://www.apachefriends.org/download.html)

Node.js should be installed : [https://nodejs.org/en/download/](https://nodejs.org/en/download/)

## Project Set up

*Clone the project into your machine.

*Create MySQL Database 

    *Queries to build database schema are in the 'sample_database.txt' file on the same repository

*Create .env file in server folder

    *Include the content at sample_env.txt file on the same repository and change environment variables values to match your local machine
	
*In the project directory, you can run:

#### Inside server folder 
### `npm install` 
Install all the npm dependencies/packages

### `npm start`
Runs the app in the development mode.\
Open [http://localhost:4000] to view it in the browser.
Server is running at http://localhost:4000

#### Inside client folder 
### `npm install`
To install all the npm dependencies

### `npm start`
Runs the app in the development mode.\
Open [http://localhost:3000] to view it in the browser.

Now the Application is running at http://localhost:3000	

## Project Implementation

Here is how the projected is implemented step-by-step

### Backend Implementation

Create a folder named 'server'

Inside that:
### `npm init`
### `npm install express --save`
Create entry point index.js file 

Install following npm dependencies/packages
### `npm install nodemon`
### `npm install mysql`
### `npm install cors`
### `npm install body-parser`
### `npm install dotenv`
### `npm install bcrypt`
### `npm install jsonwebtoken`
### `npm install dotenv`

MVC pattern is followed in server 

*Controllers - handle requests and responses

*Models - handla database related qeries

*Routes - declare all the routes/api endpoints

*Config - database configuration

*Auth - contains token_validation  

### Frontend Implementation

### `npx create-react-app client` - 

### `npm start`

Created component structure

Installed npm dependencies and packages
### `npm install react-router-dom`
### `npm install axios`
### `npm install antd`
### `npm install moment`
### `npm install prop-types`
### `npm install @ant-design/icons`

## References

Ant Pro Dashboard [https://preview.pro.ant.design/dashboard/analysis?primaryColor=%231890ff&fixSiderbar=true&colorWeak=false&pwa=false](https://preview.pro.ant.design/dashboard/analysis?primaryColor=%231890ff&fixSiderbar=true&colorWeak=false&pwa=false)
	

